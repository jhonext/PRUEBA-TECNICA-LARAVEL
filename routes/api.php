<?php

use Illuminate\Http\Request;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResources(['user' => 'API\UserController']);

Route::get('paises', 'API\PaisController@getPaises');
Route::get('estados/{pais}', 'API\PaisController@getEstados');
Route::get('ciudades/{estado}', 'API\PaisController@getCiudades');

Route::get('findUser', 'API\UserController@search');