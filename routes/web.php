<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{path}','HomeController@index')->where( 'path', '([A-z\d-/_.]+)?' );//php <7.1
Route::get('{path}','HomeController@index')->where('path','([A-z\d\-\/_.]+)?');//php7.3 >
