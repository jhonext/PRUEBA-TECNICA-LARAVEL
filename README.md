# PRUEBA TÉCNICA LARAVEL - FullStack DEVELOPER
La empresa “MAILER S.A.” desea realizar una aplicación web que permita administrar la información
de sus usuarios, cada usuario podrá realizar el envío de emails, por seguridad de la información el
usuario tendrá que autenticarse en la aplicación con sus credenciales.
## Tutorial Description 
* Sofware 
    NodeJS v14.15.1
    Laravel Framework 5.7.29

## Installation

* Clone the repo ` git clone https://github.com/jhonextgx/prueba-tecnica-laravel.git `
* `cd ` to project folder. 
* Run ` composer install `
* Save as the `.env.example` to `.env` and set your database information 
* Run ` php artisan key:generate` to generate the app key
* Run ` npm install `
* Run ` php artisan migrate ` o ` php artisan migrate --seed`
* -Execute app:
* Run ` php artisan Serve `
* Run ` npm run watch `
* Done !!! 

* Note: password: 'secret' for user admin@admin.com


