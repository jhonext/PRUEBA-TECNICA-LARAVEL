
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);


import ToggleButton from 'vue-js-toggle-button'
Vue.use(ToggleButton)


import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast = toast;


window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'));

import Vue from 'vue'
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Loading from 'vue-loading-overlay';
//Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading)

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })

let routes = [
    { path: '/home', component: require('./components/Users.vue') },
    { path: '/dashboard', component: require('./components/Users.vue') },
    { path: '/users', component: require('./components/Users.vue') },   
    { path: '*', component: require('./components/NotFound.vue') }
  ]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })



Vue.filter('festatus', function(text){
    if(text == "1"){
        return "Activo";
    }else
        return (text=="-1")?"Anulado":"Vencido"
});

Vue.filter('cobestatus', function(text){
    return (text==1)?"Activo":"Inactivo"
});


Vue.filter('pestatus', function(text){
    if(text == "1"){
        return "Siguiente";
    }else
        if(text == "0")
            return "Actual"
        else
            return (text=="-1")?"Finalizado":"Bloqueado"
});

Vue.filter('lestatus', function(text){
    if(text == "1"){
        return "Siguiente";
    }else
        return (text=="-1")?"Finalizado":"Actual"
});


Vue.filter('jganador', function(text){
    return (text==1)?"GANADOR":""
});

Vue.filter('moneda', function(text){
    return text + " BsS."
});

Vue.filter('millars', function(text){
    var flotante = parseFloat(text);
    var resultado = Math.round(flotante*100)/100;
    return resultado.toLocaleString() + " Bs."
});

Vue.filter('statusUser', function(text){
    switch(text){
        case -1: return "Eliminado"; break;
        case 0: return "Suspendido"; break;
        case 1: return "Activo"; break;
    }
});

Vue.filter('hsorteo', function(text){
        return moment(text, "hh").format('LT') ;
});

Vue.filter('BSemana', function(text){
    if(text >= 1)
    return (text==1)?"Lunes a Viernes":"Sabado y Domingo"
    else "Todos Los dias"
});

Vue.filter('tipoUser', function(text){
    if(text=="admin"){
        return "Administrador";
    }
    else{
        return (text=="author")?"Supervisor":"Estandar";
    }    
});

Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('mayus', function(text){
    return text.toUpperCase()
});

Vue.filter('myDate',function(created){
    //moment(date, 'YYYY-MM-DD').format(format);//moment(created).format('MMMM Do YYYY');
    return moment(created).format('DD/MM/YYYY');
});

Vue.filter('myDateTime',function(created){
    //moment(date, 'YYYY-MM-DD').format(format);//moment(created).format('MMMM Do YYYY');
    return moment(created).format('DD/MM/YYYY hh:mm a');
});


window.Fire =  new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue')
);


Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    router,
    data:{
        search: ''
    },
    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000),

    }
});
