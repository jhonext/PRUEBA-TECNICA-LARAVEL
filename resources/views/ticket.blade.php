<!DOCTYPE html>
<html>
<head>
    <style>
        td,
        th,
        tr,
        table {
            border-top: 1px solid black;
            border-collapse: collapse;
        }

        td.producto,
        th.producto {
            width: 70px;
            max-width: 70px;
        }

        td.cantidad,
        th.cantidad {
            width: 15px;
            max-width: 15px;
            word-break: break-all;
        }

        td.precio,
        th.precio {
            width: 50px;
            max-width: 50px;
            word-break: break-all;
        }

        .centrado {
            text-align: center;
            align-content: center;
        }

        .ticket {
                font-size: 12px;
            font-family: 'Times New Roman';
            width: 180px;
            max-width: 180px;
        }

        img {
            max-width: inherit;
            width: inherit;
        }

        @media print {
            .oculto-impresion,
            .oculto-impresion * {
                display: none !important;
            }
        }
        </style>
</head>

<body>
    <div class="ticket">
        <!--<img src="https://yt3.ggpht.com/-3BKTe8YFlbA/AAAAAAAAAAI/AAAAAAAAAAA/ad0jqQ4IkGE/s900-c-k-no-mo-rj-c0xffffff/photo.jpg" alt="Logotipo">-->        
        <p class="centrado">**** RECIBO ****
            @foreach($ticket as $tickt)
            <br>{{($tickt->macoma_hacienda==0)?'Restaurant Macoma':'Restaurant Hacienda'}}
            <br>Fecha: {{date("d/m/Y", strtotime($tickt->created_at))}}
            <br>Ticket: {{str_pad($tickt->id, 6, '0', STR_PAD_LEFT)}} Código: {{$tickt->codigo_aprobacion}}</p>
        <table>
            <thead>
                <tr>
                    <th class="producto">SERVICIO</th>
                    <th class="cantidad">PAX</th>
                    <th class="precio">VALOR</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="producto">TRASL. {{($tickt->macoma_hacienda==0)?'Macoma':'Hacienda'}} </td>
                    <td class="cantidad">{{$tickt->pax}}</td>
                    <td class="precio">{{$tickt->valor}}</td>
                </tr>

                <tr>
                    <td class="cantidad"></td>
                    <td class="producto">TOTAL</td>
                    <td class="precio">{{$tickt->valor}}</td>
                </tr>
            </tbody>
        </table>
        @endforeach
        <p class="centrado">¡GUARDE SU TICKET!
            <br>www.dpaso.pe</p>
    </div>
    <button class="oculto-impresion" onclick="imprimir()">Imprimir</button>
</body>
<script>
function imprimir() {
    window.print();
}
</script>
</html>
