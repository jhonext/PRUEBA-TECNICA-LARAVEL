<p>Hola, </p>
<h2>Alerta de asistencia No registrada:</h2>
<table border="1" width="100%" cellspacing="0" cellpadding="0.2">
    <thead>
    <tr>
        <th bgcolor="#CCCCCC" scope="col">CEDULA</th>
        <th bgcolor="#CCCCCC" scope="col">APELLIDOS Y NOMBRES</th>         
        <th bgcolor="#CCCCCC" scope="col">PROFESION</th>
        <th bgcolor="#CCCCCC" scope="col">EQUIPO</th>        
        <th bgcolor="#CCCCCC" scope="col">TIPO</th>
        <th bgcolor="#CCCCCC" scope="col">AREA</th>
		<th bgcolor="#CCCCCC" scope="col">FECHA</th>
     </tr>
  </thead>
  <tbody>
    @foreach ($data as $item)    
        <tr>
            <td>{{$item->cedula}}</td>
            <td>{{$item->nombre}} {{$item->apellido}}</td>
            <td>{{$item->profesion}}</td>
            <td>{{$item->equipo}}</td> 
            <td>{{$item->tipo}}</td>
            <td>{{$item->area}}</td>
			<td>{{$item->fecha}}</td>
        </tr>    
    @endforeach
</table>