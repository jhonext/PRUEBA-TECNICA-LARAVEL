<div class="content-container">

    <div class="container">
          <!-- Banner markup here -->

  <!--
  <div class="alert alert-dismissable alert-signed-out" id="login-or-signup-alert">
    <a class="closer" data-dismiss="alert" aria-hidden="true" href="#"><span class="icon icon-cross"></span></a>
    <div class="helper">
      <h2>The Litmus Community is the place for <strong>email designers</strong> and <strong>marketers</strong> to learn, grow, and educate each other about everything email.</h2>
      <a href="#" class="btn btn-info btn-lg toggle-sign-up-modal">Join for Free</a>
    </div>
  </div>
  -->


    </div>

    <section class="community-body">

      <div class="container">
        
  <div class="community-wrapper snippets-gallery">
    


<div class="post-container">

  <section class="post-body template-post">

    <div class="title-group inline-avatar">


      <div class="identity-avatar first-letter-c" style="height: 60px; width: 60px; line-height: 60px; font-size: 23px;"><span class="initials">CW</span></div>
      <h2 class="title">Beretun: Receipt</h2>
      <div class="metadata">
        Added by
        <a class="author" href="/p/clintonwilmott">Clinton Wilmott</a>,
        <time datetime="2016-03-30 15:11:59" data-updated-at="2017-05-24 19:44:57">5 years ago</time>
        in <a class="author" href="/community/templates/topic/33-e-commerce-templates">E-Commerce Templates</a>
      </div><!-- /meta-data -->
    </div><!-- /title-group -->

    <p>Beretun is a set of friendly, modern templates for businesses that sell online. The Receipt template is the perfect followup to any online order, providing valuable information customers need for their own records.</p>

    <div class="template-content">
      <div class="template-actions fixed" style="width: 608px; top: 20px;">
  <div class="row">
    <div class="col-lg-8">
      <ul>
          <li>
            <a class="toggle-sign-in-modal btn btn-primary download-button" data-esp="Plain HTML" href="#">
                <span class="feathericon-cloud-download"></span> Open in Litmus Builder
</a>          </li>

          <li>
            View

                  <a class="toggle-sign-in-modal u-link" href="#">Mailchimp</a>

                or

                      <a class="toggle-sign-in-modal u-link" href="#">Campaign Monitor</a>

            version.
          </li>
      </ul>
    </div>

    <div class="col-lg-4 hidden-md-down">
      <a class="tested-with-litmus" href="https://litmus.com/checklist/emails/public/2b9be3b">
        <div class="litmus-proof">
          <span class="icon-grid"></span>
          <u>Tested with Litmus</u>
        </div>
</a>    </div>

  </div>
</div><!-- /template-actions -->

    </div><!-- /span3 -->
  </section>

  <div class="template-preview">


    <img src="https://s3.amazonaws.com/community-templates/production/f28bc0a2d54bdfa09d55d3e3ce8bee6c1a80d836.png">
  </div>
</div><!-- /post-container -->

<section class="template-results">
  <div class="section-header">
    <div class="row">
      <div class="col-8">
        <h4>Similar Templates</h4>
      </div>
      <div class="col-4 text-right">
        <a class="see-all" href="/community/templates/topic/33-e-commerce-templates">
          See all
          <i class="icon-arrow-right"></i>
</a>      </div>
    </div>
  </div>

  <div class="grid">
    <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="card thumb ">
  <a href="/community/templates/28-email-monks-abandoned-cart-2">
      <div class="image">
        <img alt="Email Monks: Abandoned Cart 2" src="https://cp.litmus.com/b/?c=img&amp;l=0&amp;r=v1&amp;t=1&amp;th=280&amp;tw=280&amp;u=https%3A%2F%2Fs3.amazonaws.com%2Fcommunity-templates%2Fproduction%2F0a18fff1da9b91d6153e71ba958a5d2d93a7ad45.png">
        <div class="caption animated fadeIn"></div>
      </div>
</a>
  <div class="details">
    <a href="/community/templates/28-email-monks-abandoned-cart-2">Email Monks: Abandoned Cart 2</a>
        <div class="attr">
          <a href="/community/templates/28-email-monks-abandoned-cart-2">
              <img alt="Attribution Logo" src="https://s3.amazonaws.com/community-templates/production/2a4eaab7fd0601d555a30c6175f61181439cd5b4.jpg">
</a>        </div>
  </div>
</div>

        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card thumb ">
  <a href="/community/templates/27-email-monks-abandoned-cart-1">
      <div class="image">
        <img alt="Email Monks: Abandoned Cart 1" src="https://cp.litmus.com/b/?c=img&amp;l=0&amp;r=v1&amp;t=1&amp;th=280&amp;tw=280&amp;u=https%3A%2F%2Fs3.amazonaws.com%2Fcommunity-templates%2Fproduction%2Fad5fe998d27eab82fd384a08047956b182c0c603.png">
        <div class="caption animated fadeIn"></div>
      </div>
</a>
  <div class="details">
    <a href="/community/templates/27-email-monks-abandoned-cart-1">Email Monks: Abandoned Cart 1</a>
        <div class="attr">
          <a href="/community/templates/27-email-monks-abandoned-cart-1">
              <img alt="Attribution Logo" src="https://s3.amazonaws.com/community-templates/production/bd88ae2fec4d443198a26e71ff79916fba1bea5f.jpg">
</a>        </div>
  </div>
</div>

        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card thumb ">
  <a href="/community/templates/29-email-monks-abandoned-cart-3">
      <div class="image">
        <img alt="Email Monks: Abandoned Cart 3" src="https://cp.litmus.com/b/?c=img&amp;l=0&amp;r=v1&amp;t=1&amp;th=280&amp;tw=280&amp;u=https%3A%2F%2Fs3.amazonaws.com%2Fcommunity-templates%2Fproduction%2F251ae9edeb0b8f53c832e5e1e95b7644e5829621.png">
        <div class="caption animated fadeIn"></div>
      </div>
</a>
  <div class="details">
    <a href="/community/templates/29-email-monks-abandoned-cart-3">Email Monks: Abandoned Cart 3</a>
        <div class="attr">
          <a href="/community/templates/29-email-monks-abandoned-cart-3">
              <img alt="Attribution Logo" src="https://s3.amazonaws.com/community-templates/production/c4f1f6cfc0d570c5958563202476fd52ced24efb.jpg">
</a>        </div>
  </div>
</div>

        </div>
        <div class="col-lg-3 col-md-6">
          <div class="card thumb ">
  <a href="/community/templates/19-beretun-shopping-cart">
      <div class="image">
        <img alt="Beretun: Shopping Cart" src="https://cp.litmus.com/b/?c=img&amp;l=0&amp;r=v1&amp;t=1&amp;th=280&amp;tw=280&amp;u=https%3A%2F%2Fs3.amazonaws.com%2Fcommunity-templates%2Fproduction%2Fe6e4ac80852ed79bf4e0eb822ddc9cf3fe1e7579.png">
        <div class="caption animated fadeIn"></div>
      </div>
</a>
  <div class="details">
    <a href="/community/templates/19-beretun-shopping-cart">Beretun: Shopping Cart</a>
  </div>
</div>

        </div>
    </div><!-- /row-fluid -->
  </div>
</section>




  </div>



      </div> <!-- container -->
    </section> <!-- learning -->

  </div>