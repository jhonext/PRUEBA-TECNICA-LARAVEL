<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Rendicion_{{$date}}</title>
  <style>
    body{
        font-family: "Helvetica";
        font-size: 9pt;
    }
    table {
        width: 100%;
    }

   table td:first-child {
   width: 14.28%;
   }

   table td:nth-child(2) {
   width: 20%;
   }

   table td:nth-child(3) {
   width: 20%;
   } 

   table td:nth-child(4) {
   width: 14.28%;
   }  

   table td:nth-child(5) {
   width: 10%;  
   }         

   table td:last-child {
   width: 24%;
   }
    table tr:nth-child(even) {
      background-color: #eee;
    }

    table tr:nth-child(odd) {
      background-color: #fff;
    }    
    h3{
        font-size: 10pt;
        color: white;
        background:#cdcdcd;
        padding: 3px;
        text-align: center;
    }
    .contenedor{
        position: relative;
        display: inline-block;
        text-align: center;
    }    
    .texto-encima{
        position: absolute;
        top: 30px;
        left: 20px;
    }
    hr{
      color: #dddddd;
      height: 0px;
      border:1px dashed;
    }

    .margen_sup{
      padding-top: 10px;
    }
    #watermark1 {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   20cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   8cm;

                /** Your watermark should be behind every content
                z-index:  -1000;**/
    }
    #watermark2 {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   2cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   8cm;

                /** Your watermark should be behind every content
                z-index:  -1000;**/
    }         
  </style> 
  </head>
  <body>  
    @php
      /**       
       * @package com.jhonext.dompdf
       * @abstract Dompdf poliza
       * @author jhonext
       * @since 2008-03-04
       */
      //{{$infoPerfil->forma_pago}}    
      $meses = array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');	  
      //$aa=strftime("%Y",strtotime($date));
      //$dd=strftime("%d",strtotime($date));
      //$aa++;      
    @endphp    
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
   <thead>
      <tr>
         <th></th>
         <th></th>
         <th colspan="3">Fecha: {{$date}}</th>
         <th><!--Hora:--></th>
      </tr>
      <tr>
         <th></th>
         <th colspan="4" bgcolor="#CCCCCC" align="center" ><b>LISTADO DE PERFILES</b></th>
         <th></th>
      </tr>
      <tr>
         <th colspan="6"></th>
      </tr>
      <tr>
         <th></th>
         <th></th>
         <th colspan="4"></th>
      </tr>
      <tr>
         <th colspan="6"></th>
      </tr>
      <tr  align="center">
         <th colspan="6" ></th>
      </tr>
      <tr>
         <th colspan="6"></th>
      </tr>
      <tr>
         <th bgcolor="#CCCCCC" scope="col">CEDULA</th>
         <th bgcolor="#CCCCCC" scope="col">APELLIDOS Y NOMBRES</th>         
         <th bgcolor="#CCCCCC" scope="col">PROFESION</th>
         <th bgcolor="#CCCCCC" scope="col">EQUIPO</th>
         <!--<th bgcolor="#CCCCCC" scope="col"></th>-->
         <th bgcolor="#CCCCCC" scope="col">TIPO</th>
         <th bgcolor="#CCCCCC" scope="col">FECH. HOR.</th>
         <th bgcolor="#CCCCCC" scope="col">ACCESOS</th>
      </tr>
   </thead>
   <tbody>
      <!--repetitivo -->      
      @foreach ($infoPerfil['persona'] as $item)
      <tr>
         <td>{{$item->cedula}}</td>
         <td>{{$item->nombres}}</td>
         <td>{{$item->profesion}}</td>
         <td>{{$item->equipo}}</td>     
         <td>{{$item->accion}}</td>
         <td>{{$item->fecha_asistencia}}</td>
         <td>             
            @php 
               echo $infoPerfil['areas']->where('id', $item->asistioa/*$infoPerfil['accesos']->where('idpersona', $item->id)->pluck('idarea')*/)->pluck('nombre')->first();
             @endphp
         </td>
      </tr>
      @endforeach
      <!-- en repetitivo -->
      <!-- Totalizado -->
 
      </tr>
      <!-- PIE --> 
      <tr>
         <td colspan="3"></td>
         <td aling='right'><b>GENERADO POR:</b></td>
         <td colspan="2">{{$infoPerfil['usuario']}}</td>
      </tr>
   </tbody>
</table>
  </body>
</html>
