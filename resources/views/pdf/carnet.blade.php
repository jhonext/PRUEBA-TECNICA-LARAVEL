<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Persona{{$data['persona']->cedula}}</title>
  <style>
    body{
        font-family: "Helvetica";
        font-size: 10pt;    
    }
    table {
        width: 100%;        
    }
    h3{
        font-size: 10pt;
        color: white;
        background:#cdcdcd;
        padding: 3px;
        text-align: center;
    }
    .contenedor-ppal{      
        position: relative;
        display: inline-block;
        text-align: center;        
    } 
    .contenedor{      
        position: relative;
        display: inline-block;
        text-align: center;     
    }         
    .texto-encima{
        position: absolute;
        top: 80px;
        left: 130px;
    }
    hr{
      color: #dddddd;
      height: 0px;
      border:1px dashed;
    }

    .margen_sup{
      position: fixed;
      padding-top: 80px;
      padding-left: 50px;
    }
    #qr {
      position: fixed;
      top:   8.2cm;
      left:     2.2cm;

      /** Change image dimensions**/
      width:    2.1cm;
      height:   1.9cm;
      z-index: 1;
    }
    #foto {
    position: fixed;

    /** 
      Set a position in the page for your image
      This should center it vertically
    **/
    top:  5.1cm;
    left: 2.1cm;

    /** Change image dimensions**/
    width:    2.3cm;
    height:   2.6cm;
     z-index: 1;
    } 
    #acceso {
      position: fixed;
      top:  8.2cm;
      left: 4.9cm;

      /** Change image dimensions**/
      width:    2.3cm;
      height:   2.6cm;      
      z-index: 1;
    }
         
  </style> 
  </head>
  <body class="margen_sup">  
    @php
      /**       
       * @package com.jhonext.dompdf
       * @abstract Dompdf poliza
       * @author jhonext
       * @since 2008-03-04
       */
      //{{$data->forma_pago}}
      //$meses = array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');	  
      //$aa=strftime("%Y",strtotime($date));
      //$dd=strftime("%d",strtotime($date));
      //$aa++; 
      $cantAccesos = count ($data['accesos']);
      $heith = ($cantAccesos>=4)?23:50;
    @endphp
 
    <br>
    <br>
    @if( $data['persona']->foto != "" )
  
      <div id="foto">
      <img src="http://superligavzla.com/img/profile/{{$data['persona']->foto}}" height="100%" width="100%" />
      </div>
      <div id="qr">        
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')/*->size(90)*/->generate($data['persona']->qr)) !!} " height="100%" width="100%"/>
     </div>

     <div id="acceso">
      <table style="width:100%">
        <tr>
          @if ($cantAccesos == 0)
            <td style="background-color:white; width:100;">No Posee Acceso</td>
          @elseif ($cantAccesos === 1)
        <td style="background-color:#{{ $data['accesos'][0]->color }};width:100;" height="{{$heith}}">&nbsp;</td>
          @elseif ($cantAccesos === 2)
            <td style="background-color:#{{ $data['accesos'][0]->color }}; width:50;"height="{{$heith}}">&nbsp;</td>
            <td style="background-color:#{{ $data['accesos'][1]->color }}; width:50;">&nbsp;</td>
          @else
            <td style="background-color:#{{ $data['accesos'][0]->color }};width:33;" height="{{$heith}}">&nbsp;</td>
            <td style="background-color:#{{ $data['accesos'][1]->color }}; width:33; ">&nbsp;</td>
            <td style="background-color:#{{ $data['accesos'][2]->color }}; width:33;">&nbsp;</td>          
          @endif  
        </tr>
        @if( $cantAccesos >= 4 )
        <tr>
          @if ($cantAccesos == 4)
            <td style="background-color:#{{ $data['accesos'][3]->color }}; width:33;" height="{{$heith}}">&nbsp;</td>
            <td style="background-color:#fff; width:33;">&nbsp;</td>
            <td style="background-color:#fff; width:33;">&nbsp;</td>
          @elseif ($cantAccesos >= 5)            
            <td style="background-color:#{{ $data['accesos'][3]->color }}; width:33;" height="{{$heith}}">&nbsp;</td>
            <td style="background-color:#{{ $data['accesos'][4]->color }}; width:33;">&nbsp;</td>
            <td style="background-color:#fff; width:33;">&nbsp;</td>
          @endif 
        </tr>
        @endif
      </table>
     </div>
    @endif    
   <div class="contenedor-ppal"> 
      <table cellspacing="3">
        <tr>
          <td>
            <div class="contenedor">                    
              <img src="./img/carnet_LA.jpg" alt="carnet superliga" width="307" height="444" border="0" /> 
              <div class="texto-encima">
                <table style="font-size:7pt; color:#333;">
                  <tr>
                    <td WIDTH="140">
                      Cédula: <strong>{{$data['persona']->cedula}}</strong>                                            
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      Nombre y Apellido: <br><strong>{{$data['persona']->nombres}}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Profesión: <br> <strong>{{$data['persona']->profesion}}</strong>
                    </td>
                    <td>
                      
                    </td>                          
                  </tr>
                  <tr>
                    <td colspan="2">
                      Equipo: <br><strong>{{$data['persona']->equipo}}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                     
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                     
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="font-size:9pt; left:8px; color:#fff; font-weight:bold">
                     
                    </td>
                  </tr>
                </table>
              </div>
              <img src="./img/carnet_LB.jpg" alt="carnet insulcars" width="307" height="444" border="0" />
            </div>                  
          </td>               
        </tr>
      </table>
     
      <!--<table WIDTH="90">
        <tr>
          <td>
            <div class="contenedor">                    
              <img src="./img/carnet_LA.jpg" alt="carnet insulcars" width="307" height="444" border="0" /> 
              <div class="texto-encima">
                <table style="font-size:9pt; color:#333; font-weight:bold">
                  <tr>
                    <td WIDTH="140">
                      {{$data['persona']->nombres}}
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      Equipo: {{$data['persona']->equipo}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Profesion: {{$data['persona']->profesion}}
                    </td>
                    <td>
                      
                    </td>                          
                  </tr>
                  <tr>
                    <td colspan="2">
                      
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                     
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                     
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="font-size:9pt; left:8px; color:#fff; font-weight:bold">
                     
                    </td>
                  </tr>
                </table>
              </div>
              <img src="./img/carnet_LB.jpg" alt="carnet insulcars" width="307" height="444" border="0" />
            </div>                  
          </td>               
        </tr>
      </table>-->
</div>
  </body>
</html>