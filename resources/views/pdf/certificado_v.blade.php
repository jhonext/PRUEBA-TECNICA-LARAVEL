<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Certificado_{{ $dataCertificado['nro_certificado'] }}</title>
  <style type="text/css">

    @font-face {
      font-family: "cremona";
      src: url("./fonts/cremona/cremona_regular.ttf");
    }
    @font-face {
      font-family: "JosefinSans-ligth";
      src: url("./fonts/josefinsans/JosefinSans-Light.ttf");
    }
    @font-face {
      font-family: 'JosefinSans';
      font-style: normal;
      font-weight: 400;
      src: url("./fonts/josefinsans/JosefinSans-Regular.ttf");  
    }
    @font-face {
      font-family: 'JosefinSans-SemiBold';
      font-style: normal;
      font-weight: 400;
      src: url("./fonts/josefinsans/JosefinSans-SemiBold.ttf");
    }
    
    .pagina{
      position: relative;
      width: 100%;     
    }
    .imagenFondo{
      position: absolute;
      width: 100%;
      height: 1100px;
      z-index: -1;
    }
    .centrado{
      text-align: center;
    }
    .titulo{
      position: relative;
      font-family: 'cremona';
      font-size: 25pt;
      color: #e58334;
      padding-top: 15px;
	    padding-bottom: 25px;
	    text-align: center;
    }
    .texto_n{
      font-family: 'JosefinSans-ligth';
      font-style: normal;
      font-size: 12pt;
      color: #797A7B;				
    }
    .alumno{
      font-family: "cremona";
      font-style: normal;
      font-weight: 400;
      color: #2d5052;
      padding: 0.4em;
      
    }
    
    .curso{
      font-size: 15pt;
      font-family: "JosefinSans-SemiBold";
      font-style: normal;
      font-weight: 400;
      color: #2d5052;
    }
    .der{
      float: right;     
	    margin-left: 80%;
    }
    .competente{
      font-family: "JosefinSans-SemiBold";
      font-size: 11pt;
      color: #2d5052;
    }


    /********/
    .exemplar{	
      font-family: "JosefinSans-SemiBold";
      font-size: 14pt;
      font-style: normal;
      color: #2d5052;
    } 
    
    
    .email{
      font-family: 'JosefinSans-SemiBold';
      font-style: normal;
      color: #2d5052;
      font-size: 12pt;
              
    }
    .email2{   
      position: relative;  
      font-family: 'JosefinSans-ligth';
      font-style: normal;
      font-size: 14pt;
      text-align: right;
      color: #fff;
      
      
    }	  
  
    .izq{
      text-align: left;
    }
    
    ul {
      list-style: none;
      padding: 0px;
      margin-left: 5px;
    }
      
    ul li::before {
      content: "\2022";  
      color: #e58334;
      font-size: 58px;
      line-height: 30px;
      vertical-align: -11px;
    }
    p{
      padding: 0px !important;
      margin-top: 0 !important ;
      margin-bottom: 5px !important;
    }
    hr{
      color: black !important;
      border: 2px solid !important;
      border-radius: 5px !important;
    }
    #watermark1 {
      position: fixed;
      /** 
        Set a position in the page for your image
        This should center it vertically
        */
      bottom:   -2cm;
      left:     -2cm;
      /** Change image dimensions*/
      width:    23cm;
      height:   30cm;
      /** Your watermark should be behind every content**/
      z-index:  -1000;
    }

    #watermark2 {
        position: fixed;
        bottom:   11.4cm;
        left:     14.5cm;
        /** Change image dimensions**/
        width:    8cm;
        height:   8cm;
    }

  </style>

</head>

<body >
  
  <div id="watermark1">
    <img src="./img/m1/unido.fw.png" height="100%" width="100%" />
  </div>   
  <div id="watermark2">
    <img src="./img/m1/r3.png" alt="" width="155" height="150"/>
  </div>
   
    <table border="0">
      <tbody>
        <tr>
          <td class="centrado" colspan="4"><img src="./img/m1/titulo.png" width="150" height="124" alt=""/></td>
        </tr>
        <tr>
          <td class="titulo" colspan="4">&nbsp;&nbsp;&nbsp;CERTIFICADO DE ENTRENAMIENTO</td>
        </tr>

        <tr>
          <td colspan="4" class="texto_n">Certificamos que:</td>
        </tr>
        <tr>
          <td class="alumno" colspan="4">{{ $dataCertificado['nombre'] }}</td>          
        </tr>
        <tr>
          <td class="texto_n" colspan="4">Ha culminado satisfactoriamente el curso de:</td>         
        </tr>
       
        <tr>
          <td colspan="4" class="curso">{{ $dataCertificado['titulo_1'] }}<br>{{ $dataCertificado['titulo_2'] }}<br>{{ $dataCertificado['descripcion'] }}</td>

        </tr>
       
        <tr>
          <td class="texto_n" colspan="4">Y ha sido evaluado como "competente" en la/s unidad/es comprendidas:</td>
        </tr>

        <tr class="texto_n posicion" >
          <td colspan="4">
            <div >
              <ul>
                <li>Exemplar Global EM : {{ $dataCertificado['global_em'] }}</li>
                <li>Exemplar Global AU : {{ $dataCertificado['global_au'] }}</li>
                <li>Exemplar Global TL : {{ $dataCertificado['global_tl'] }}</li>
              </ul>  
           </div>
          </td>
        </tr>

        <tr class="exemplar">
          <td colspan="3">
            <div>
              <p>Certificado N°:    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $dataCertificado['nro_certificado']  }} </p>
              <p>Fecha de Examén :  &nbsp;&nbsp;&nbsp;{{ $dataCertificado['fecha_examen'] }} </p>
              <p>Fecha de Emisión : &nbsp;&nbsp;&nbsp;{{ $dataCertificado['fecha_emision'] }}</p>
            </div>
          </td>
          <td class="der" colspan="1" align="right"><img src="./img/m1/r4.png" width="180" height="85" alt=""/></td>
        </tr>       

        <tr>
          <td colspan="2"><center> <img src="./img/m1/firma.png" width="160" height="62" alt=""/></center><hr></td>
          <td colspan="2">&nbsp;</td>
        </tr>

        <tr class="texto_n">
          <td colspan="4">Emitido en nombre de la ESCUELA</td>         
        </tr>
        <tr class="texto_n">
          <td colspan="4">DE AUDITORES DEL PERU by INTERCERT</td>
        </tr>
        <tr class="texto_n">
          <td colspan="4">Jefe de Entrenamiento</td>  
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>  
        </tr>		
        <tr class="texto_n">
          <td colspan="4">Este certificado ha sido emitido a nombre de la</td>
        </tr>
        <tr class="texto_n">
          <td colspan="3">ESCUELA DE AUDITORES DEL PERÚ by INTERCERT </td>
          <td colspan="1">&nbsp;</td>
        </tr>
        <tr class="texto_n">
          <td colspan="4">bajo la aprobacion de Exemplar Global, en caso de ser</td>
        </tr>
        <tr class="texto_n">
          <td colspan="4">requerido puede ser verificado en:</td>
        </tr>
        <tr class="email">
          <td colspan="4">info@intercert.com</td>
        </tr>
        <tr>
          
          <td  colspan="4"></td>
        </tr>
      </tbody>
    </table>
    <div class="email2">www.escueladeauditores.edu.pe</div>
</body>
</html>