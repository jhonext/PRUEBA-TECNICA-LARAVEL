<?php
namespace App\Repository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Config;
use Carbon\Carbon;

class TipoClienteRepository
{
   protected $TipoCliente;
    public function __construct()
    {

    }

    public function getAllTipoCliente(){
        DB::statement("SET sql_mode = '' ");
        
        $filtroNombre = \Request::get('nombre')?? false;                  
           

        $tipoClientes = DB::table('tipo_cliente')
                        ->select('tipo_cliente.*')
                        ->where(function($query) use($filtroNombre){

                            if($filtroNombre){                                
                               $query->where('nombre','like','%'.$filtroNombre.'%');
                            }                      
                        })
                        ->latest()
                        ->paginate(20);
        return $tipoClientes;
    }

    public function getTipoClienteSelect(){
        $TipoCliente = DB::table('tipo_cliente')->select('id as code', 'nombre as label')
        ->orderby('id', 'asc')
        ->get();

        return $TipoCliente;
    }
    //Devuelve un tipo_cliente por id
    public function getByIdTipoCliente($id)
    {
        return DB::table('tipo_cliente')->where('id',$id)->get();
    }

    //Agrega un registro en la tabla tipo_cliente
    public function addTipoCliente($data)
    {
        $dataTipoCliente = [
            'nombre' => Str::upper($data['nombre']),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()         
        ];
     
        return DB::table('tipo_cliente')->insert($dataTipoCliente)?1:0;
    }

    //Actualiza un registro en la tabla Cliente
    public function updateTipoCliente($data, $id)
    {
        $dataTipoCliente = [
            'nombre' => Str::upper($data['nombre']),
            'updated_at' => Carbon::now()         
        ];
        $estatus = DB::table('tipo_cliente')->where('id', $id)->update($dataTipoCliente);
        return $estatus;       
    }

    //Borra un Cliente
    public function deleteTipoCliente($id){
        $estatus = DB::table('tipo_cliente')
                        ->where('id', $id)
                        ->delete();
        return $estatus;
    }    
}