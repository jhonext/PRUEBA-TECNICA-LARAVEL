<?php
namespace App\Repository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Config;
use Carbon\Carbon;

class ClienteRepository
{
   protected $Clientes;
    public function __construct()
    {

    }

    public function getClientes(){
        DB::statement("SET sql_mode = '' ");
        
        $filtroNombre = \Request::get('nombre')?? false;
        $filtroEmail =  \Request::get('email') ?? false;     
        $filtroOrganizacion = \Request::get('organizacion') ?? false;
        $filtroTipoId = \Request::get('tipo_id') ?? false;               
     
        $clientes = DB::table('cliente')
                        ->select('cliente.*','tipo_cliente.nombre as tipocliente')
                        ->join('tipo_cliente','tipo_cliente.id','=','cliente.tipo_id')                                              
                       ->where(function($query) use($filtroEmail, $filtroOrganizacion, $filtroTipoId, $filtroNombre){

                            if($filtroNombre){                                
                               $query->where('cliente.nombre','like', '%'.$filtroNombre.'%');
                            }

                             if($filtroEmail){                                
                                $query->where('cliente.email','like', '%'.$filtroEmail.'%');
                            }                             

                           if($filtroOrganizacion){                                
                                $query->where('cliente.organizacion','like', '%'.$filtroOrganizacion.'%');
                            }
                            
                            if($filtroTipoId){
                                if($filtroTipoId != '-1')                            
                                    $query->where('cliente.tipo_id','=',$filtroTipoId);
                            }                       
                        })
                        ->latest()
                        ->paginate(20);
        
        return $clientes;
    }

    public function getClientesSelect(){
        $Clientes = DB::table('cliente')->select('cliente.*')
        ->orderby('id', 'desc')
        ->get();

        return $Clientes;
    }
    //Devuelve un Cliente por id
    public function getByIdCliente($id)
    {
        return DB::table('cliente')->where('id',$id)->get();
    }

    //Agrega un registro en la tabla Cliente
    public function addCliente($data)
    {
        $dataCliente = [
            'nombre' => Str::upper($data['nombre']),
            'apellido' => Str::upper($data['apellido']),
            'email' => $data['email'],
            'organizacion' => $data['organizacion'],
            'num_telefonico' => $data['num_telefonico'],
            'tipo_id' => $data['tipo_id'],
            'descripcion' => $data['descripcion'],            
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()         
        ];
     
        return DB::table('cliente')->insert($dataCliente)?1:0;
    }

    //Actualiza un registro en la tabla Cliente
    public function updateCliente($data, $id)
    {
        $dataCliente = [
            'nombre' => Str::upper($data['nombre']),
            'apellido' => Str::upper($data['apellido']),
            'email' => $data['email'],
            'organizacion' => $data['organizacion'],
            'num_telefonico' => $data['num_telefonico'],
            'tipo_id' => $data['tipo_id'],
            'descripcion' => $data['descripcion']?$data['descripcion']:"",
            'updated_at' => Carbon::now()
        ];
        $estatus = DB::table('cliente')->where('id', $id)->update($dataCliente);
        return $estatus;       
    }

    //Borra un Cliente
    public function deleteCliente($id){
        $estatus = DB::table('cliente')
                        ->where('id', $id)
                        ->delete();
        return $estatus;
    }    
}