<?php

namespace App\Traits;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Carbon\Carbon;

trait LotApiDb{

    protected $Client;

    public function __construct()
    {
      
    }
    //Paginacion personalizada
    public function paginate($items, $perPage = 15, $page = null,
    $baseUrl = null,
       $options = [])
      {
          $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

          $items = $items instanceof Collection ?
                         $items : Collection::make($items);

          $lap = new LengthAwarePaginator($items->forPage($page, $perPage),
                             $items->count(),
                             $perPage, $page, $options);

          if ($baseUrl) {
              $lap->setPath($baseUrl);
          }

          return $lap;
    }

    //cachear Datos id para cacheo, data no codificada
    public function CacheoData($idcacheo, $data){
        if (!\Cache::has($idcacheo)) {
            \Cache::put($idcacheo,json_decode($data->getBody()->getContents()), 20);
        }
        return \Cache::get($idcacheo);
    }

}
