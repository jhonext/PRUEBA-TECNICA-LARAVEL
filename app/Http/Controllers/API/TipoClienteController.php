<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\TipoClienteRepository;

class TipoClienteController extends Controller
{
    protected $oTipoCliente;
    public function __construct(TipoClienteRepository $oTipoCliente)
    {       
        $this->oTipoCliente = $oTipoCliente;
    } 
    
    public function selectTipoCliente(){
        return  response()->json($this->oTipoCliente->getTipoClienteSelect());      
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->oTipoCliente->getAllTipoCliente());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre' => 'required',          
        ]);
        $responseData = $this->oTipoCliente->addTipoCliente($request->all());
        return response()->json([
                'message' => $responseData==1?'Tipo Cliente Guardado Correctamente!!':'Los datos no se Guardaron',
            ], $responseData==1?200:400);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombre' => 'required',          
        ]);

        $responseData = $this->oTipoCliente->updateTipoCliente( $request->all(), $id);

        return response()->json([
                'message' => $responseData?'Tipo Cliente Actualizado Correctamente!!':'Los datos no se Actualizaron',
            ], $responseData?200:400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responseData = $this->oTipoCliente->deleteTipoCliente($id);
        return response()->json([
                'message' => $responseData==1?'Tipo Cliente Eliminado Correctamente!!':'Los datos no se Eliminaron',
            ], $responseData==1?200:400);
    }
}
