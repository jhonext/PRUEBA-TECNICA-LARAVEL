<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorios\ConfiguracionRepositorio;

class ConfiguracionController extends Controller
{
    protected $oConfiguracion;
    public function __construct(ConfiguracionRepositorio $oConfiguracion)
    {
        $this->oConfiguracion = $oConfiguracion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->oConfiguracion->getConfiguracion();
    }

    public function getNroConfiguracion($user_cod){
        return $this->oConfiguracion->getNroConfiguracion($user_cod);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            "clase" => 'required',
            "grupo" => 'required',
            "cobertura" => 'required',

            "forma_pago" => 'required',
            "fecha_emision" => 'required',
        ],[
            'cedula_rif.required' => "Ingrese la cedula o rif.",
            'nombre_cli.required' => "Ingrese el nombre.",
           // 'valor.required' => "Debe indicar el valor del ticket",
        ]);        
        return  $this->oConfiguracion->addConfiguracion($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->oConfiguracion->getConfiguracion($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $this->validate($request,[

        "email_for" => 'required',
        "email_bcc" => 'required',             
    ],[
        'email_for.required' => "Ingrese Email destinatario.",
        'email_bcc.required' => "Ingrese Email Bcc.",       
    ]);        
    return  $this->oConfiguracion->updateConfiguracion( $id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // return  $this->oConfiguracion->deleteConfiguracion($id);
    }

    public function search(){
        return  $this->oConfiguracion->search();
    }
    


}
