<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositorios\ExportExcelCertificadoRepositorio;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function exportCertificados(){
        return Excel::download(new ExportExcelCertificadoRepositorio, 'Listado_certificados.xlsx');
    }
}
