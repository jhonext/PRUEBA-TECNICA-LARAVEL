<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositorios\PersonaRepositorio;
use App\Repositorios\PerfilRepositorio;
use App\Repositorios\AsistenciaRepositorio;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PdfController extends Controller
{
    protected $oCarnet, $oPerfil, $oAsistencia;
    public function __construct(PersonaRepositorio $oCarnet, PerfilRepositorio $oPerfil, AsistenciaRepositorio $oAsistencia)
    {
        $this->oCarnet = $oCarnet;
        $this->oPerfil = $oPerfil;
        $this->oAsistencia = $oAsistencia;
    }

    public function reportePerfil() 
    {        
        try{ 
            $meses = array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');	  
            $infoPerfil = $this->oPerfil->getAllPerfilReporte();
            $date = Carbon::now()->toDateString();
            //dd($infoPerfil);
            $view =  \View::make('pdf.perfil', compact('infoPerfil','date'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setPaper('A4','landscape');//, 'landscape' portrait
            $pdf->loadHTML($view);

            return $pdf->stream('Reporte_Perfil.pdf');
        }catch (Throwable  $e){
            return "Informacion no disponible";
        }          
    }

    public function reporteResumen(){
        return  $this->oAsistencia->getAsistenciaResumen();
    }
    
    public function reporteAsistencia() 
    {   
        try{      
            $meses = array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');	  
            $infoPerfil = $this->oAsistencia->getAllAsistenciaReporte();
            //dd($infoPerfil);
            $date = Carbon::now()->toDateString();           
            $view =  \View::make('pdf.asistencia', compact('infoPerfil','date'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setPaper('A4','landscape');//, 'landscape' portrait
            $pdf->loadHTML($view);
            //-------------------- 

            //------------------------------------------
            //return $pdf->download('Carnet_'.$nroCarnet.'.pdf');
            return $pdf->stream('Reporte_Perfil.pdf'); 
        }catch (Throwable  $e){
            return "Informacion no disponible";
        }               
    }     
    

    public function carnet($idPersona) 
    {
       try{ 
        $data = $this->getData($idPersona);  
        $view =  \View::make('pdf.carnet', compact('data'/*, 'date', 'constantes','poliza','dataAdic','formaPago'*/))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('letter');
        $pdf->loadHTML($view);
        //------------------------------------------
            return $pdf->stream('Carnet_'.$data['persona']->cedula.'.pdf');        
        }catch (Throwable  $e){
            return "Informacion no disponible";
        }
    }   

    public function getData($idPersona) 
    {   
        return  $this->oCarnet->getCarnetPersona($idPersona);
    }

    public function pathDelImagen($imagen){
        return public_path('img'.DIRECTORY_SEPARATOR.'profile').DIRECTORY_SEPARATOR.$imagen;
    }

    public function verArchivos(){
        $personas = $this->oCarnet->getPersonas();
        $data = "";        
        foreach ($personas as $persona) {      
            if(\File::exists($this->pathDelImagen($persona->foto))){
                //echo "n exist"; die();
            } else {  $data.= "".$persona->nombre." ".$persona->apellido.";".$persona->equipo." <br>";   }  
        }
        $data.="";

        echo $data;
    }

}
