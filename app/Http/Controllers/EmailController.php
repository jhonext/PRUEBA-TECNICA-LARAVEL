<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositorios\AsistenciaRepositorio;
use App\Repositorios\ConfiguracionRepositorio;
use Illuminate\Support\Str;

class EmailController extends Controller
{

	protected $oAsistencia;
	protected $oConfiguracion;
    public function __construct(AsistenciaRepositorio $oAsistencia, ConfiguracionRepositorio $oConfiguracion)
    {
		$this->oAsistencia = $oAsistencia;
		$this->oConfiguracion = $oConfiguracion;
    }

    public function contacto(){
		$data = $this->oAsistencia->Alerta8am();
		
		//$config = $this->oConfiguracion->getConfiguracion();	
		//if($data){
			$subject = "Test";
			$for = 'jhonext@gmail';//$config->pluck('email_for')->first();
			$bcc = 'jorgeonline140182@gmail';//$config->pluck('email_bcc')->first();
			$msj = "hola mundo";
			\Mail::send('email/email', ['data' => $data],function($msj) use($subject,$for,$bcc){
				$msj->from("sistemaacceso@ciudadguaiqueri.com","Notificacion de asistencia");            
				$msj->subject($subject);
				$msj->to($for);
				$msj->bcc($bcc);
			});
			return response()->json(['success' => 1, 'message' => "Mensaje enviado"]);
		//}
		///return response()->json(['success' => 0, 'message' => "No data"]);
    }	

    public function contact(){
		$data = $this->oAsistencia->Alerta8am();
		
		$config = $this->oConfiguracion->getConfiguracion();	
		if($data){
			$subject = "Sistema de notificaciones";
			$for = $config->pluck('email_for')->first();
			$bcc = $config->pluck('email_bcc')->first();
			\Mail::send('email/email',['data' => $data], function($msj) use($subject,$for,$bcc){
				$msj->from("superligavzla@gmail.come","Notificacion de asistencia");            
				$msj->subject($subject);
				$msj->to($for);
				$msj->bcc($bcc);
			});
			return response()->json(['success' => 1, 'message' => "Mensaje enviado"]);
		}
		return response()->json(['success' => 0, 'message' => "No data"]);
    }

    public function avisoEntrada($email_for="jhonext@gmail.com"){
		$data = ['msg' => "Gracias por su Compra"];$this->oAsistencia->Alerta8am();
		
		//$config = $this->oConfiguracion->getConfiguracion();	
		if($data){
			$subject = "Guaiqueries - Venta Online";
			$for = $email_for;//$config->pluck('email_for')->first();
			$bcc = "jorgeonline140182@gmail.com";//$config->pluck('email_bcc')->first();
			\Mail::send('email/email_compraOnline',['data' => $data], function($msj) use($subject,$for,$bcc){
				$msj->from("superligavzla@gmail.come","Compra de Entrada");            
				$msj->subject($subject);
				$msj->to($for);
				$msj->bcc($bcc);
			});
			return response()->json(['success' => 1, 'message' => "Mensaje enviado"]);
		}
		return response()->json(['success' => 0, 'message' => "No data"]);
    }	
}
