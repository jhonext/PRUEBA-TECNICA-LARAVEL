<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('cedula', 11)->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('password_verify');
            $table->string('type')->default('user');
            $table->string('celular', 10);
            $table->date('fecha_nacimiento');
            $table->integer('codigo_ciudad')->nullable();
            $table->integer('estatus')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
